import SwiftUI

struct ContentView: View {
    @State private var settings = Settings()

    var body: some View {
        VStack {
            Text("Settings:")
                .font(.largeTitle)
            SettingsScene(settings: self.$settings)
            Divider()
            Text("Form:")
                .font(.largeTitle)
            FormScene(settings: self.$settings)
            Spacer()
        }
    }
}

#Preview {
    ContentView()
}
