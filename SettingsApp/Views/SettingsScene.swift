import SwiftUI

struct SettingsScene: View {
    @Binding var settings: Settings

    var body: some View {
        Toggle("Show Section", isOn: self.$settings.showSection)
    }
}

#Preview {
    @State var settings = Settings()
    return SettingsScene(settings: $settings)
}
