import SwiftUI

struct FormScene: View {
    @Binding var settings: Settings

    var body: some View {
        Form {
            Section {
                Text("The stuff in the first section.")
            } header: {
                Text("Always Show This Section")
            }
            if settings.showSection {
                Section {
                    Text("The stuff in the second section.")
                } header: {
                    Text("Conditional Section")
                }
            }
        }
    }
}

#Preview {
    @State var settings = Settings()
    return FormScene(settings: $settings)
}
